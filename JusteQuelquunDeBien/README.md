# Juste Quelqu'un de Bien - Arrangement for Polyband Rhythmsection

Just a few comments for the different parts:

## General

If you know how to play something to sound better, do it. You are the expert on your instrument.

## Voice

I tried to get the rhythm (and text) as close as possible to the original. However, as the singer sings agogically quite freely, it is probably best to still listen to the recording two or three times to get the feeling right.

## Guitar

In the chorus, the guitar player plays this very rhythmic kind of muted pattern. As I had no clue about how to write that down, I just filled the bars with slashes and noted the chords. I think that you know better how to make it sound awesome. Play a bit agocially freely.

## Bass

Again, I tried to write down the baseline as close as possible to the original. In the recording, however, the bass sounds quite "muffeled", which firstly makes it difficult for me to get the right pitch, and secondly allows for more jazzy dissonance. There are some places where my baseline sounds weird but the original works just fine with the same notes (see e.g. the C#/E# chords). Therefore: If it is possible to play a bit muffeled, that would be awesome.

## Piano

"Ad lib." means comping (but sparsely) in a low position. Play a bit agocially freely.

## Drums

The drummer plays almost the same bar throughout the piece. Everything which changes from that pattern is written down. Play with brushes.

# License

This work is licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/) by Alexander Schoch, which means that you are allowed to do with it whatever you want and license it under whatever license you like. You just have to give me appropriate credit.


